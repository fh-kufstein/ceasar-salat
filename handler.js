const serverless = require("serverless-http");
const express = require("express");
const app = express();

app.get("/", (req, res, next) => {
  let text = req.query.text;
  let key = req.query.key;

  let result = caesarCipher(text, key);


  return res.status(200).json({
    message: result
  });
});

app.use((req, res, next) => {
  return res.status(404).json({
    error: "Not Found",
  });
});

module.exports.handler = serverless(app);


function caesarCipher(word, next) {
  next = next % 26; let res = ""; for (const letter of word) {

    let letterCode = letter.charCodeAt(0);
    if (letterCode >= 65 && letterCode <= 90) {
      letterCode = letterCode + next;
      if (letterCode > 90) {
        letterCode = letterCode - 26;
      } else if (letterCode < 65) {
        letterCode = letterCode + 26;
      }
    } else if (letterCode >= 97 && letterCode <= 122) {
      letterCode = letterCode + next;

      if (letterCode > 122) {
        letterCode = letterCode - 26;
      } else if (letterCode < 97) {
        letterCode = letterCode + 26;
      }
    }

    res = res + String.fromCharCode(letterCode);
  }

  return res;
}